

const add_line_to_plot = (svg, x, y, data, color='black') => {
    svg.append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", color)
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
        .x(function(d) { return x(d.x) })
        .y(function(d) { return y(d.y) })
        );
}


const add_points_to_plot = (svg, x, y, data, color='black', opacity=1) => {
    svg
        .append("g")
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", function(d) { return x(d.x) } )
        .attr("cy", function(d) { return y(d.y) } )
        .attr("r", 5)
        .attr("fill", (d)=>color)
        .attr("opacity", opacity);
}


const plot_line = (div_destination, title, data) => {
    var margin = {top: 10, right: 30, bottom: 30, left: 60},
    width = 460 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select(div_destination)
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    svg.append('text')
        .attr('x', width/3)
        .attr('y', 5 * margin.top)
        .text(title)


    var x = d3.scaleTime()
        .domain(d3.extent(data, function(d) { return d.x; }))
        .range([ 0, width ]);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    var y = d3.scaleLinear()
    .domain(d3.extent(data, function(d) { return d.y; }))
    .range([ height, 0 ]);
    svg.append("g")
        .call(d3.axisLeft(y));

    add_points_to_plot(svg, x, y, data, 'black', 0.5);
    add_points_to_plot(svg, x, y, average_by_week(data), 'red');
    add_line_to_plot(svg, x, y, average_by_week(data), 'red');

}


const sort_data = (data, time_column) => {
    return data.sort((d1, d2)=>d1[time_column]-d2[time_column]);
}


const day_duration = 1000 * 60 * 60 * 24;


const round_date_to_day = (date) => {
    var coeff = day_duration;
    return new Date(Math.round(date.getTime() / coeff) * coeff);
}


const increment_date = (date, duration_in_millis) => {
    return new Date(date.getTime() + duration_in_millis);
}



const average_by_week = (data, time_column='x', value_column='y') => {
    let min_date = round_date_to_day(data[0][time_column]);
    let max_date = round_date_to_day(data[data.length - 1][time_column]);
    let avg_data = new Array();
    let cur_date = min_date;
    while (cur_date < max_date) {
        let next_date = increment_date(cur_date, 7 * day_duration);
        let data_this = data.filter(x=>
            (x[time_column]>=cur_date && x[time_column]<next_date)
        )
        let sum_value = data_this.reduce((p, c)=>p + parseFloat(c[value_column]), 0);
        avg_data = [...avg_data, {x: cur_date, y: sum_value/data_this.length}];
        cur_date = next_date;
    }
    return avg_data;
}


// const make_plots = async (dbModule, filter='all') => {
//     $('#chart').empty();
//     data_all = await dbModule.selectAllFromTable('latest_core_om_results');
//     if (filter === 'non-full'){
//         data_all = data_all.filter(x=>!x.is_complete_survey);
//     } else if (filter === 'full'){
//         data_all = data_all.filter(x=>x.is_complete_survey);
//     } else { 
//         data_all = data_all;
//     }
//     dimensions = Array.from(new Set(data_all.map(x=> x.dimension))) 
//     data_all = sort_data(data_all
//         .map((x) => Object.assign(x, {
//             x: new Date(Date.parse(x.created_at)),
//             y: parseInt(x.score),
//             c: d3.schemeCategory10[dimensions.indexOf(x.dimension)]
//         })), 'x');

//     plot_line('#chart', 'all dimensions', data_all);

//     for (dimension of dimensions) {
//         let data = data_all.filter(x=>x.dimension == dimension);

//         plot_line('#chart', dimension, data);
//     }
// }


export {
    sort_data,
    plot_line
}