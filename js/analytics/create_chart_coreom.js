import { sort_data, plot_line } from './plot_utils.js';


const make_button_primary = (jqueryBtn) => {
    jqueryBtn.addClass('btn-primary');
    jqueryBtn.removeClass('btn-secondary');
}


const make_button_secondary = (jqueryBtn) => {
    jqueryBtn.removeClass('btn-primary');
    jqueryBtn.addClass('btn-secondary');
}


const make_plots = async (dbModule, filter='all') => {
    $('#chart').empty();
    let data_all = await dbModule.selectAllFromTable('latest_core_om_results');
    if (filter === 'non-full'){
        data_all = data_all.filter(x=>!x.is_complete_survey);
    } else if (filter === 'full'){
        data_all = data_all.filter(x=>x.is_complete_survey);
    } else { 
        data_all = data_all;
    }
    let dimensions = Array.from(new Set(data_all.map(x=> x.dimension))) 
    data_all = sort_data(data_all
        .map((x) => Object.assign(x, {
            x: new Date(Date.parse(x.created_at)),
            y: parseInt(x.score) / 4.0,
            c: d3.schemeCategory10[dimensions.indexOf(x.dimension)]
        })), 'x');

    plot_line('#chart', 'all dimensions', data_all);

    for (let dimension of dimensions) {
        let data = data_all.filter(x=>x.dimension == dimension);

        plot_line('#chart', dimension, data);
    }
}


import('../dbInterface.js').then((dbModule) => {
    make_plots(dbModule);
    $('#toggle-scope').append(`
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <button class="btn btn-primary" type="radio" name="options" id="optionAll" autocomplete="off" checked> All  </button>
        <button class="btn btn-secondary" type="radio" name="options" id="optionFull" autocomplete="off" checked> Full </button>
        <button class="btn btn-secondary" type="radio" name="options" id="optionRandom" autocomplete="off" checked> Random </button>
    </div>
    `);
    $("#optionAll").on("click", ()=>{
        make_button_primary($("#optionAll"));
        make_button_secondary($("#optionFull"));
        make_button_secondary($("#optionRandom"));
        make_plots(dbModule, 'all'); 
    });
    $("#optionFull").on("click", ()=>{
        make_button_secondary($("#optionAll"));
        make_button_primary($("#optionFull"));
        make_button_secondary($("#optionRandom"));
        make_plots(dbModule, 'full'); 
    });
    $("#optionRandom").on("click", ()=>{
        make_button_secondary($("#optionAll"));
        make_button_secondary($("#optionFull"));
        make_button_primary($("#optionRandom"));
        make_plots(dbModule, 'non-full'); 
    });
})