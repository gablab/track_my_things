import { sort_data, plot_line } from './plot_utils.js';


const make_plots = async (dbModule) => {
    $('#chart').empty();
    let data_all = await dbModule.selectAllFromTable('glycaemia');
    data_all = sort_data(data_all
        .map((x) => Object.assign(x, {
            x: new Date(Date.parse(x.created_at)),
            y: parseInt(x.data.glycaemia),
        })), 'x');

    plot_line('#chart', 'glycaemia', data_all);
}


import('../dbInterface.js').then((dbModule) => {
    make_plots(dbModule);
})