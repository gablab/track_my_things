const table_glycaemia = 'glycaemia';

import { insertIntoTable, getUserId } from '../dbInterface.js';
import { resetSurvey, resetQuestions } from '../formUtils.js';

const formName = 'glycaemia';
const togglePlugin = () => {
    resetSurvey($("#survey"), submitForm);
    $('#survey-options').append(`<a href='analytics_glycaemia.html'><button class='btn btn-light'>show analytics</button></a>`);
    generateglycaemia();
}


const append_question = (jquery_parent) => {
    jquery_parent.find("form").append(`
        <div>
            <label class="form-check-label">Glycaemia value</label>
            <input type="text" class="form-control" id="valueglycaemia"></input>
        </div>
    `);
}


const generateglycaemia = async () => {
    resetQuestions($("#survey-questions"), submitForm);
    append_question($("#survey-questions"));
}


const submitForm = async () => {
    let user_id = await getUserId();
    let results = $.map($("#survey-questions").find("#valueglycaemia"), (a)=>{
        return {
            user_id: user_id,
            data: {
                glycaemia: $(a).val(),
            }
        }
    });
    console.log(results);
    insertIntoTable(table_glycaemia, results);
    document.querySelectorAll('input[type=text]').forEach((x) => { x.value = ''; });
}


export {
    formName,
    togglePlugin,
};
