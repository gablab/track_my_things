const table_questions = 'core_om_questions';
const table_scores = 'core_om_results';


import {selectAllFromTable, insertIntoTable, getUserId} from '../dbInterface.js';
import { resetQuestions, resetSurvey } from '../formUtils.js';

const formName = 'Core Om';
const togglePlugin = async () => {
    resetSurvey($("#survey"), submitForm);
    generateSurveyTypeSelection();
    generateRandomQuestion();
    //generateFullSurvey();
}

const core_om_get_questions = () => {
    return selectAllFromTable(table_questions);
}


const generateSurveyTypeSelection = () => {
    $('#survey-options').append(`<br/><button class='btn btn-light'>show full survey</button>`).on('click', generateFullSurvey);
    $('#survey-options').append(`<a href='analytics_coreom.html'><button class='btn btn-light'>show analytics</button></a>`);
    $('#survey-options').append(`<p>Nell'ultima settimana:</p>`);
}



const append_question = (jquery_parent, question) => {
    let values = question.is_higher_better ? [4, 3, 2, 1, 0] : [0, 1, 2, 3, 4];
    jquery_parent.find("form").append(`
        <p>${question.id}. ${question.text_it}</p>
        <div class="mb-3 radio-container" id="coreOm_${question.id}">
            <!--<label class="form-check-label">Risposta: </label>-->

            <div class="form-check">
            <input class="form-check-input q${question.id}" type="radio" name="q${question.id}_radioEntry" id="q${question.id}_radioOption0" value="${values[0]}">
            <label class="form-check-label q${question.id}" for="q${question.id}_radioOption0">0 - Per nulla</label>
            </div>       
            
            <div class="form-check">
            <input class="form-check-input q${question.id}" type="radio" name="q${question.id}_radioEntry" id="q${question.id}_radioOption1" value="${values[1]}">
            <label class="form-check-label q${question.id}" for="q${question.id}_radioOption1">1 - Solo occasionalmente</label>
            </div>

            <div class="form-check">
            <input class="form-check-input" q${question.id} type="radio" name="q${question.id}_radioEntry" id="q${question.id}_radioOption2" value="${values[2]}">
            <label class="form-check-label q${question.id}" for="q${question.id}_radioOption2">2 - A volte</label>
            </div>

            <div class="form-check">
            <input class="form-check-input q${question.id}" type="radio" name="q${question.id}_radioEntry" id="q${question.id}_radioOption3" value="${values[3]}">
            <label class="form-check-label q${question.id}" for="q${question.id}_radioOption3">3 - Spesso</label>
            </div>

            <div class="form-check">
            <input class="form-check-input q${question.id}" type="radio" name="q${question.id}_radioEntry" id="q${question.id}_radioOption4" value="${values[4]}">
            <label class="form-check-label q${question.id}" for="q${question.id}_radioOption4">4 - Molto spesso o sempre</label>
            </div>

        </div>

    `);
}

const generateRandomQuestion = async () => {
    let questions = await core_om_get_questions();
    let question = questions[Math.floor(Math.random() * questions.length)];
    resetQuestions($("#survey-questions"), submitForm);
    append_question($("#survey-questions"), question);
}

const generateFullSurvey = async () => {
    let questions = await core_om_get_questions();
    resetQuestions($("#survey-questions"), submitForm);
    for (let question of questions) {
        append_question($("#survey-questions"), question);
    }
}


 const submitForm = async () => {
    for (let rc of Array.from(document.querySelectorAll('.radio-container'))){
      let is_at_least_one_selected = Array.from(document.querySelectorAll('.form-check-input'))
        .filter(x=>x.parentNode.parentNode.id == rc.id)
        .reduce((acc, cur)=>acc + cur.checked, false);
      if (! is_at_least_one_selected) {
        alert('missing entries!');
        return;
      }
    }
    let user_id = await getUserId();
    let is_complete_survey = $("#survey-questions").find(".radio-container").length == 34;
    let results = $.map($("#survey-questions").find(":checked"), (a)=>{
        return {
            user_id: user_id,
            question_id: $(a).parent().parent().attr("id").split('_')[1],
            data: {
                score: $(a).val(),
                is_complete_survey: is_complete_survey,
            }
        }
    });
    if (is_complete_survey) alert(
        'total score: ',
        results.reduce((p, c) => {return parseInt(c.data.score) + p}, 0)
    );
    //console.log(results);
    insertIntoTable(table_scores, results);
    document.querySelectorAll('input[type=radio]').forEach((x) => { x.checked = false; });
    generateRandomQuestion();
}


export {
    formName,
    togglePlugin,
};
