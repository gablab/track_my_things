// https://github.com/supabase/supabase-js#esm
import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';
// import {getCookie} from './cookies.js';
import { supabaseKey, supabaseUrl } from './secrets/supabase.js';

var supabase = createClient(supabaseUrl, supabaseKey);

const authSupabase = async (event) => {
    //event.preventDefault();

    supabase.auth
    .signOut()
    .then((_response) => {
        console.log('logout successful')      
    })
    .catch((err) => {
      console.log(err.response.text);
    });

    const supabaseUser = $('#supabaseUsername').val();
    const supabasePassword = $('#supabasePassword').val();
    //console.log(supabaseUser, supabasePassword);


    supabase.auth
        .signInWithPassword({ email: supabaseUser, password: supabasePassword })
        .then((response) => {
            console.log(response);
            response.error ? alert(response.error.message) : console.log('login successful', response.data.session.access_token, response.data.session.refresh_token);
            getUserId().then(x=>console.log('user_id: ', x));
        })
        .catch((err) => {
            console.log(err);
        });
}
$('button#supabaseLogin').on("click", authSupabase);

setInterval(()=>{
    getUserId().then(x=>{
        let loginButton = $('button#supabaseLogin');
        if (x === undefined){
            loginButton.removeClass('btn-success').addClass('btn-danger');
        } else {
            loginButton.removeClass('btn-danger').addClass('btn-success');
        }
    });
    getUserName().then(x=>{
        if (x === undefined){
            $('#login-status').text('not logged in').attr('style', 'color:red');
        } else {
            $('#login-status').text(`logged in as ${x}`).attr('style', 'color:black');
        }
    });



}, 1000);

const selectAllFromTable = async (tableName) => {
    const { data, errorSelect } = await supabase
        .from(tableName)
        .select();
  
    return data;
}


// record can be either a list or a single value
const insertIntoTable = async (tableName, record) => {
    const { errorInsert } = await supabase
        .from(tableName)
        .insert(record);
}


const getUserId = async () => {
    let userdata = await supabase.auth.getUser();
    return supabase.auth.getUser().then((x) => x.data.user.id);
}

const getUserName = async () => {
    let userdata = await supabase.auth.getUser();
    return supabase.auth.getUser().then((x) => x.data.user.email);
}


export {
    selectAllFromTable,
    insertIntoTable,
    getUserId
};