// const saveCredentials = () => {
//     const supabaseUser = $('#supabaseUsername').val();
//     const supabaseUrl = $('#supabaseUrl').val();
//     const supabaseKey =  $('#supabaseKey').val();
//     document.cookie = `supabaseUser=${supabaseUser}`;
//     document.cookie = `supabaseUrl=${supabaseUrl}`;
//     document.cookie = `supabaseKey=${supabaseKey}`;
// }
// $("form#supabaseAuth").find("button#saveSupabaseCredentials").on("click", saveCredentials);


for (let formModulePath of [
    './plugins/core_om.js',
    './plugins/glycaemia.js',
]){
    import(formModulePath).then((form) => {
        let thisId = form.formName.replace(' ', '_');
        $("div#formToggles").append(`<button id="toggle${thisId}" class='btn btn-outline-primary'>toggle ${form.formName}</button>`)
        $(`button#toggle${thisId}`).on("click", form.togglePlugin);
    }
    );
}
