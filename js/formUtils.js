const resetSurvey = (jqueryParent, submitForm) => {
    jqueryParent.empty();
    jqueryParent.append(`<div id='survey-options'></div>`);
    jqueryParent.append(`<div id='survey-questions'></div>`);
}

const resetQuestions = (jqueryParent, submitForm) => {
    jqueryParent.empty();
    jqueryParent.append(`<form></form>        <button type="button" class="btn btn-primary" id="send_form">Send</button>
    `);
    jqueryParent.find("#send_form").on("click", submitForm);
}


export {
    resetSurvey,
    resetQuestions,
};